﻿using System;
using System.Collections;
using System.Collections.Generic;

using MyStack;

namespace Application
{
    class Application
    {

        static void enumerate<T>(CustomStack<T> stack)
        {
            foreach(T val in stack)
            {
                Console.WriteLine("Enumerated value: {0}", val);
            }
        }
        static void Main(string[] args)
        {
            String divider = "------------------------------------------------------";
            CustomStack<int> MyStack = new CustomStack<int>();
            Console.WriteLine("Custom Stack created.");
            try{
                Console.WriteLine("Trying to peek into empty stack... {0}", MyStack.Peek());
            }
            catch{
                Console.WriteLine("Handling exception. Peeking into empty stack is not possible.");
            }
            MyStack.Push(1);
            MyStack.Push(2);
            MyStack.Push(6);
            MyStack.Push(5);
            Console.WriteLine(divider);
            Console.WriteLine("Trying to enumerate all values in stack:");
            enumerate<int>(MyStack);
            Console.WriteLine(divider);
            Console.WriteLine("Retrieved value: {0}", MyStack.Peek());
            Console.WriteLine(divider);
            Console.WriteLine("Current size: {0}", MyStack.Count);
            int val = MyStack.Pop();
            Console.WriteLine("Value popped: {0}, new size: {1}, new latest value: {2}", val, MyStack.Count, MyStack.Peek());
            Console.WriteLine("Trying to enumerate all values in stack:");
            enumerate<int>(MyStack);
            Console.WriteLine(divider);

            Console.WriteLine("Creating CustomStack by providing only size:");
            CustomStack<int> MyStack2 = new CustomStack<int>(5);
            Console.WriteLine("Successfully created. Enumerating it:");
            enumerate<int>(MyStack2);
            Console.WriteLine(divider);

            Console.WriteLine("Creating custom stack by providing an array:");
            CustomStack<int> MyStack3 = new CustomStack<int>( new int[]{1,2,3,4,5,6});
            Console.WriteLine("Successfully created. Enumerating it:");
            enumerate<int>(MyStack3);
            Console.WriteLine(divider);

            Console.WriteLine("Trying to enumerate int stack as string/double/float/bool or any other data type:");
            //enumerate<double>(MyStack);
            Console.WriteLine("Custom function `enumerate` is not possible due to conflicting generic type.");
            Console.WriteLine("Trying manually:");
            try{
                foreach(double value in MyStack)
                {
                    Console.WriteLine("Enumerated value: {0}", value);
                }
            }
            catch{
                Console.WriteLine("Handling exception. As a result, this is not possible.");
            }
            Console.WriteLine("Enumerating manually doesn't do anything different. I'm getting InvalidCastException.");
            Console.WriteLine(divider);

            Console.WriteLine("Testing stack to work with other data types:");
            CustomStack<string> MyStack4 = new CustomStack<string>();
            MyStack4.StackChange += HandleStackEvent;
            MyStack4.Push("Hello");
            MyStack4.Push("World");
            MyStack4.Push("From");
            MyStack4.Push("My");
            MyStack4.Push("Stack");
            MyStack4.Push("Realization");

            enumerate<string>(MyStack4);
            Console.WriteLine(divider);

            Console.WriteLine("Current stack max size: {0}", MyStack4.MaxSize);
            MyStack4.MaxSize = 5;
            Console.WriteLine("New stack max size: {0}", MyStack4.MaxSize);
            enumerate<string>(MyStack4);
            Console.WriteLine(divider);

            Console.WriteLine("Trying to push more values to stack.");
            try{
                MyStack4.Push("Test");
            }
            catch(ArgumentOutOfRangeException)
            {
                System.Console.WriteLine("Failed to add more values.");
            }
            enumerate<string>(MyStack4);
            Console.WriteLine(divider);

            MyStack4.MaxSize = 7;
            Console.WriteLine("New stack max size: {0}", MyStack4.MaxSize);
            enumerate<string>(MyStack4);

            Console.WriteLine(divider);

            Console.WriteLine("Checking, if stack has word \'Hello\': {0}", MyStack4.Contains("Hello"));
            Console.WriteLine(divider);

            string[] temp_array = new string[8];
            Console.WriteLine("Trying to copy internals of stack to temp array.");
            MyStack4.CopyTo(temp_array, 4);

            foreach(string t in temp_array)
            {
                Console.WriteLine("Enumerated value: {0}", t);
            }
            Console.WriteLine(divider);

            Console.WriteLine("Clearing MyStack4.");
            MyStack4.Clear();
            enumerate<string>(MyStack4);
            Console.WriteLine(divider);
            
            Console.WriteLine(divider);
            Console.WriteLine("Our work is done here. MyStack successfully tested.");
            // Stack<int> OrigStack = new Stack<int>();

        }

        private static void HandleStackEvent<T>(object sender, CustomEventArgs<T> args)
        {
            Console.WriteLine(args.message);
        }
    } 
}
